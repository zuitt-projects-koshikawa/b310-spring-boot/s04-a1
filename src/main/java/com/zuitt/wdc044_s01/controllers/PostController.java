package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import com.zuitt.wdc044_s01.services.PostService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@RestController // Handle HTTP Request and generating appropriate Responses
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;


    // Create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization")String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(@RequestHeader(value = "Authorization")String stringToken){

        Iterable<Post> allPosts = postService.getPost();
        return new ResponseEntity<>(allPosts, HttpStatus.OK);
    }
}
